crypted backups
===============

| **crypted backups** enables for |xz|-compressed and |gnupg| encrypted backups of certain configuration data by the use of |systemd| |systemd.service| and |systemd.timer|.
| System-wide and user-specific backups are possible. The script and all of its components are licensed under the GPLv3.

| **Note:** Most of **crypted backups** was designed with a headless system in mind.
| **Note:** By default the script uses **/tmp/** - on most systems a volatile file-system (**tmpfs**) - as a working space! Depending on the size of your (singular) backups and available RAM you might want to consider moving this working space somewhere else.
| **Note:** Due to systemd's architecture you can copy/paste and modify given default timers from **/usr/lib/systemd/{systemd,user}/** and put them to **/etc/systemd/{system,user}/**. The latter will always have precedence over the former. Same counts for services!
| **Note:** To minimize the size of your user backups, you might want to consider using tools like |profile-cleaner| to shrink the size of your program profiles.

Requirements
------------

* |xz|
* |gnupg|

System backups
--------------
| The configuration file for system services can be found in **/etc/crypted-backups**. 
| Here you can set the public key to encrypt with, source and destination folder for backups and cleanup policy.
| The following backups are possible (have a look at the default settings in the configuration, to get an idea, about what does what).

* |aura| package states
* |bitlbee| configuration
* |git| repositories
* |maildir| & |mailman| configuration
* |mariadb| databases
* /var/log folder
* /var/log/journal folder
* websites folder

Example
_______
| In this example the system backup for the bitlbee configuration will be started by a timer.
| This timer unit has the same name as its service.

  .. code:: bash

    /usr/lib/systemd/system/backup-bitlbee.service
    /usr/lib/systemd/system/backup-bitlbee.timer

| Using **systemctl** one can easily start and stop timer units belonging to a service. As root:

  .. code:: bash

    systemctl start backup-bitlbee.timer
    systemctl stop backup-bitlbee.timer

To make the system boot with this timer started, enable it. Again, as root:

  .. code:: bash

    systemctl enable backup-bitlbee.timer
    systemctl disable backup-bitlbee.timer

| When the timer's conditions are met, the service **backup-bitlbee.service** is started automatically. Much like cron.
| Unlike cron, you can also start and stop the service separately:

  .. code:: bash

    systemctl start backup-bitlbee.service
    systemctl stop backup-bitlbee.service

User backups
------------
| The configuration file for user services is read first from **~/.crypted-backups** followed by **~/.config/crypted-backups**. Environment variables set in the first file will be replaced by the ones set in the latter, if present.
| Take a look at the example file in **/usr/share/doc/crypted-backups/crypted-backups.conf.example** to get an idea of how to use the services.
| Here you can set the public key to encrypt with, source and destination folder for backups and cleanup policy.
| The following pbackups

* |firefox| profile backup
* |thunderbird| profile backup

Example
_______
| In this example the user backup for a user's |firefox| profile will be started by a timer (like in the example above).

  .. code:: bash

    /usr/lib/systemd/user/backup-firefox.service
    /usr/lib/systemd/user/backup-firefox.timer

| Using **systemctl** one can easily start and stop timer units belonging to a service. This time, as your user:

  .. code:: bash

    systemctl --user start backup-firefox.timer
    systemctl --user stop backup-firefox.timer

| To make your user environment start with this timer started, enable it. Again, as your user:

  .. code:: bash

    systemctl --user enable backup-firefox.timer
    systemctl --user disable backup-firefox.timer

| When the timer's conditions are met, the service **backup-firefox.service** is started (as your user) automatically.
| This service can of course also just be started and stopped separately from the timer:

  .. code:: bash

    systemctl --user start backup-firefox.service
    systemctl --user stop backup-firefox.service


.. |xz| raw:: html

  <a href="http://tukaani.org/xz/" target="_blank">xz</a>

.. |gnupg| raw:: html

  <a href="https://gnupg.org" target="_blank">GnuPG</a>

.. |systemd| raw:: html

  <a href="https://en.wikipedia.org/wiki/Systemd" target="_blank">systemd</a>

.. |systemd.service| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/systemd.service.html" target="_blank">services</a>

.. |systemd.timer| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/systemd.service.html" target="_blank">timers</a>

.. |profile-cleaner| raw:: html

  <a href="https://aur.archlinux.org/packages/profile-cleaner/" target="_blank">profile-cleaner</a>

.. |aura| raw:: html

  <a href="https://github.com/fosskers/aura" target="_blank">aura</a>

.. |bitlbee| raw:: html

  <a href="https://www.bitlbee.org/" target="_blank">bitlbee</a>

.. |git| raw:: html

  <a href="http://git-scm.com/" target="_blank">git</a>

.. |maildir| raw:: html

  <a href="https://en.wikipedia.org/wiki/Maildir" target="_blank">Maildir</a>

.. |mailman| raw:: html

  <a href="http://list.org" target="_blank">mailman</a>

.. |mariadb| raw:: html

  <a href="https://mariadb.com/" target="_blank">MariaDB</a>

.. |firefox| raw:: html

  <a href="https://www.mozilla.org/en/firefox/" target="_blank">Firefox</a>

.. |thunderbird| raw:: html

  <a href="https://www.mozilla.org/en/thunderbird/" target="_blank">Thunderbird</a>


